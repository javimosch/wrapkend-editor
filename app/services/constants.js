module.exports = async app => {
	var self = {
		fileTypes: ['javascript', 'function', 'middleware', 'pug', 'route', 'markdown', 'css', 'service', 'schema']
	}
	return self;
}